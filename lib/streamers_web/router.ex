defmodule StreamersWeb.Router do
  use StreamersWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", StreamersWeb do
    pipe_through :api

    get "/movies", MovieController, :index
  end

  scope "/api", StreamersWeb do
    pipe_through :api
  end
end
