defmodule StreamersWeb.MovieController do
  use StreamersWeb, :controller

  def index(conn, _params) do
    render conn, movies: []
  end
end
