defmodule StreamersWeb.MovieView do
  use StreamersWeb, :view

  def render("index.json", %{movies: movies}) do
    movies
  end
end
