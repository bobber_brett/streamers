defmodule Streamers.MovieTest do
  use StreamersWeb.MovieView
  use ExUnit.Case, async: true
  use Plug.Test
  alias Streamers.Movie

  @valid_attrs %{name: "some name", rating: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Movie.changeset(%Movie{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Movie.changeset(%Movie{}, @invalid_attrs)
    refute changeset.valid?
  end
end
